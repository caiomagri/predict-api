import unittest
import numpy as np
from unittest.mock import MagicMock, patch
from fastapi.testclient import TestClient

from .main import app
from .classifier import Classifier


class TestClassifier(unittest.TestCase):
    def setUp(self):
        # Create a mock model
        self.model = MagicMock()
        self.classifier = Classifier(self.model)

    @patch("app.classifier.Image")
    @patch("app.classifier.img_to_array")
    def test_predictions(self, mock_img_to_array, mock_image):
        # Prepare mock data
        file_data = b"mock image data"
        mock_img = MagicMock()
        mock_img.convert.return_value = mock_img
        mock_img.resize.return_value = mock_img
        mock_img_to_array.return_value = np.zeros((1, 32, 32, 3), dtype=np.float32)
        self.model.predict.return_value = np.array([[0.8, 0.2]])  # Mock predictions

        mock_image.open.return_value.__enter__.return_value = mock_img

        # Call the predictions method
        result = self.classifier.predictions(file_data)

        # Assertions
        self.assertTrue(self.model.predict.called)
        self.assertEqual(result.shape, (1, 2))
        np.testing.assert_array_equal(result, np.array([[0.8, 0.2]]))

    def test_get_predict_classification(self):
        predictions = np.array([[0.8, 0.2]])  # Mock predictions

        # Call the get_predict_classification method
        result = self.classifier.get_predict_classification(predictions)

        # Assertion
        self.assertEqual(result, 0)

    def test_get_predicted_description(self):
        predicted_class = 0  # Mock predicted class

        # Call the get_predicted_description method
        result = self.classifier.get_predicted_description(predicted_class)

        # Assertion
        self.assertEqual(result, "Trem")


class TestClassifierEndpoint(unittest.TestCase):
    def setUp(self):
        # Create a mock model
        self.model = MagicMock()
        self.classifier = MagicMock()
        self.model.predict.return_value = np.array([[0.8, 0.2]])  # Mock predictions
        self.classifier.predictions.return_value = np.array([[0.8, 0.2]])
        self.classifier.get_predict_classification.return_value = 0
        self.classifier.get_predicted_description.return_value = "Trem"

    def test_predict_endpoint(self):
        # Mock the Classifier class
        with patch("app.main.Classifier", return_value=self.classifier):
            client = TestClient(app)
            file_data = b"mock file data"
            response = client.post(
                "/predict", files={"file": ("mock_file.jpg", file_data, "image/jpeg")}
            )

            # Assertions
            self.assertEqual(response.status_code, 200)
            data = response.json()
            self.assertEqual(data["classification"], 0)
            self.assertEqual(data["description"], "Trem")
            self.assertEqual(data["predictions"], [[0.8, 0.2]])


class TestHealthCheck(unittest.TestCase):
    def setUp(self):
        self.client = TestClient(app)

    def test_health_check(self):
        response = self.client.get("/health_check")
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json(), {"message": "alive"})
